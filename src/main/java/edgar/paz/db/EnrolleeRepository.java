package edgar.paz.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnrolleeRepository extends JpaRepository<EnrolleeEntity, String> {

    @Query("select e from EnrolleeEntity e where e.parent = ?1")
    List<EnrolleeEntity> findByParent(EnrolleeEntity parent);
}
