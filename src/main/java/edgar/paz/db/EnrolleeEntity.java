package edgar.paz.db;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "Enrollee")
public class EnrolleeEntity {
    @Id
    String id;

    @ManyToOne
    EnrolleeEntity parent;

    String name;

    String phoneNumber;

    Boolean activationStatus;

    Date birthDate;

    @OneToMany(mappedBy="parent")
    private Collection<EnrolleeEntity> children;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public EnrolleeEntity getParent() {
        return parent;
    }

    public void setParent(EnrolleeEntity parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActivationStatus() {
        return activationStatus;
    }

    public void setActivationStatus(Boolean activationStatus) {
        this.activationStatus = activationStatus;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Collection<EnrolleeEntity> getChildren() {
        return children;
    }

    public void setChildren(Collection<EnrolleeEntity> children) {
        this.children = children;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
