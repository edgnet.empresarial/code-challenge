package edgar.paz.util;

import edgar.paz.db.EnrolleeEntity;
import edgar.paz.dto.Enrollee;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Optional;

public class EnrolleeUtil {

    static final SimpleDateFormat DATE_FORMAT_YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd");

    public static Enrollee optEntityToDTO(Optional<EnrolleeEntity> enrolleeOpt){
        Enrollee enrollee = null;
        if(enrolleeOpt.isPresent()){
            enrollee = entityToDTO(enrolleeOpt.get());

        }

        return enrollee;
    }

    public static Enrollee entityToDTO (EnrolleeEntity enrolleeEntity){
        Enrollee enrollee = new Enrollee();
        enrollee.setId(enrolleeEntity.getId());
        enrollee.setName(enrolleeEntity.getName());
        String parent = enrolleeEntity.getParent() == null ? null : enrolleeEntity.getParent().getId();
        enrollee.setParent(parent);
        enrollee.setActivationStatus(enrolleeEntity.getActivationStatus());
        enrollee.setBirthDate(enrolleeEntity.getBirthDate());

        return enrollee;
    }

    public static EnrolleeEntity dtoToEntity(Enrollee enrollee){
        EnrolleeEntity entity = new EnrolleeEntity();

        entity.setId(enrollee.getId());
        entity.setName(enrollee.getName());
        if(enrollee.getParent() != null){
            EnrolleeEntity parent = new EnrolleeEntity();
            parent.setId(enrollee.getParent());
            entity.setParent(parent);
        }
        entity.setActivationStatus(enrollee.getActivationStatus());
        entity.setPhoneNumber(enrollee.getPhoneNumber());
        entity.setBirthDate(enrollee.getBirthDate());

        return entity;
    }
}
