package edgar.paz.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import edgar.paz.db.EnrolleeEntity;
import edgar.paz.db.EnrolleeRepository;
import edgar.paz.dto.Enrollee;
import edgar.paz.util.EnrolleeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EnrollmentService {

    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    EnrolleeRepository enrolleeRepository;

    public Enrollee getEnrollee(String id){
        Enrollee enrollee =  EnrolleeUtil.optEntityToDTO(enrolleeRepository.findById(id));

        if(enrollee != null){
            EnrolleeEntity parent = new EnrolleeEntity();
            parent.setId(enrollee.getId());
            List<EnrolleeEntity> dependents = enrolleeRepository.findByParent(parent);
            enrollee.setDependents(dependents.stream().map(EnrolleeEntity::getId).collect(Collectors.toList()));
        }


        return enrollee;
    }

    public String saveEnrollee(Enrollee newEntollee){
        EnrolleeEntity newEntity = EnrolleeUtil.dtoToEntity(newEntollee);

        enrolleeRepository.save(newEntity);

        return "OK";
    }

    public void removeEnrollee(Enrollee enrollee){
        enrolleeRepository.deleteById(enrollee.getId());
    }

    @Transactional
    public void addDependents(String idEnrollee, String... dependents){
        Optional<EnrolleeEntity> optEntity = enrolleeRepository.findById(idEnrollee);
        if(optEntity.isPresent()){
            EnrolleeEntity entity = optEntity.get();
            for(String dependent : dependents){
                Optional<EnrolleeEntity> optDependant = enrolleeRepository.findById(dependent);
                if(optDependant.isPresent()){
                    EnrolleeEntity dependant = optDependant.get();
                    dependant.setParent(entity);
                    enrolleeRepository.save(dependant);
                }else{
                    throw new RuntimeException("Dependant not found");
                }
            }
        }else{
            throw new RuntimeException("Enrollee doesnt exists");
        }
    }

    @Transactional
    public void removeDependents(String idEnrollee, String... dependents){
        Optional<EnrolleeEntity> optEntity = enrolleeRepository.findById(idEnrollee);
        if(optEntity.isPresent()){
            EnrolleeEntity entity = optEntity.get();
            for(String dependent : dependents){
                Optional<EnrolleeEntity> optDependant = enrolleeRepository.findById(dependent);
                if(optDependant.isPresent()){
                    EnrolleeEntity dependant = optDependant.get();
                    if(dependant.getParent() != null && dependant.getParent().getId().equals(entity.getId())){
                        dependant.setParent(null);
                        enrolleeRepository.save(dependant);
                    }else{
                        throw new RuntimeException("Dependant not asociated to enrollee");
                    }

                }else{
                    throw new RuntimeException("Dependant not found");
                }
            }
        }else{
            throw new RuntimeException("Enrollee doesnt exists");
        }
    }


    @Transactional
    public void updateDependents(String idEnrollee, String... dependents){
        Optional<EnrolleeEntity> optEntity = enrolleeRepository.findById(idEnrollee);
        if(optEntity.isPresent()){
            EnrolleeEntity entity = optEntity.get();
            removeDependents(entity.getId(),
                    (String[])enrolleeRepository.findByParent(entity).
                            stream().map(EnrolleeEntity::getId).
                            collect(Collectors.toList()).toArray()
            );
        }else{
            throw new RuntimeException("Enrollee doesnt exists");
        }
    }



    @PostConstruct
    void test() throws Exception{


        EnrolleeEntity e1 = new EnrolleeEntity();
        e1.setId("1");

        enrolleeRepository.save(e1);


        EnrolleeEntity e2 = new EnrolleeEntity();
        e2.setId("2");
        e2.setParent(e1);

        enrolleeRepository.save(e2);

        List<EnrolleeEntity> lst = enrolleeRepository.findAll();
        System.out.println(lst.size() + " elementos");


        EnrolleeEntity e3 = new EnrolleeEntity();
        e3.setId("3");

        enrolleeRepository.save(e3);

        //System.exit(0);

    }
}
