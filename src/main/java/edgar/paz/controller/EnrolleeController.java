package edgar.paz.controller;

import edgar.paz.dto.UpdateDependentsRequest;
import edgar.paz.dto.Enrollee;
import edgar.paz.service.EnrollmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/enrollee")
public class EnrolleeController {

    @Autowired
    EnrollmentService enrollmentService;

    @GetMapping("/status/{id}")
    public Enrollee getStatus(@PathVariable String id){
        return enrollmentService.getEnrollee(id);
    }

    @PostMapping("/save")
    public String postSave(@RequestBody  Enrollee enrollee){
        return enrollmentService.saveEnrollee(enrollee);
    }

    @PostMapping("/addDependents")
    public String postAddDependents(@RequestBody UpdateDependentsRequest request){
        enrollmentService.addDependents(request.getEnrolleeId(), request.getDependents());
        return "OK";
    }

    @PostMapping("/removeDependents")
    public String postRemoveDependents(@RequestBody UpdateDependentsRequest request){
        enrollmentService.removeDependents(request.getEnrolleeId(), request.getDependents());
        return "OK";
    }

    @PostMapping("/updateDependents")
    public String postUpdateDependents(@RequestBody UpdateDependentsRequest request){
        enrollmentService.updateDependents(request.getEnrolleeId(), request.getDependents());
        return "OK";
    }
}
