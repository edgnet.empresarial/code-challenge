package edgar.paz.dto;

public class UpdateDependentsRequest {
    String enrolleeId;
    String[] dependents;

    public String getEnrolleeId() {
        return enrolleeId;
    }

    public void setEnrolleeId(String enrolleeId) {
        this.enrolleeId = enrolleeId;
    }

    public String[] getDependents() {
        return dependents;
    }

    public void setDependents(String[] dependents) {
        this.dependents = dependents;
    }
}
